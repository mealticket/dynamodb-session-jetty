package com.mealticket.jetty.session.dynamodb;

import org.eclipse.jetty.server.session.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;

import java.util.LinkedHashSet;
import java.util.Set;

public class DynamoSessionDataStore extends AbstractSessionDataStore {
    private final DynamoDbTable<DynamoSession> table;
    private final CurrentTimeProvider currentTimeProvider;

    private static final Logger logger = LoggerFactory.getLogger(DynamoSessionDataStore.class);

    DynamoSessionDataStore(DynamoDbTable<DynamoSession> table) {
        this(table, new CurrentTimeProvider());
    }

    DynamoSessionDataStore(DynamoDbTable<DynamoSession> table, CurrentTimeProvider currentTimeProvider) {
        this.table = table;
        this.currentTimeProvider = currentTimeProvider;
    }

    /**
     * @see org.eclipse.jetty.server.session.AbstractSessionDataStore#doStore(String, SessionData, long)
     */
    public void doStore(String s, SessionData sessionData, long lastSaveTime) throws Exception {
        this.table.putItem(new DynamoSession(sessionData));
        logger.debug("Saved session {} to dynamodb", sessionData.getId());
    }

    /**
     * @see org.eclipse.jetty.server.session.SessionDataStore#getExpired(Set)
     */
    public Set<String> doGetExpired(Set<String> candidates) {
        Set<String> expire = new LinkedHashSet<>();
        long now = this.currentTimeProvider.getCurrentTime();

        for(String candidate : candidates) {
            try {
                SessionData existingSession = doLoad(candidate);
                if(existingSession == null) {
                    expire.add(candidate);
                } else if (_context.getWorkerName().equals(existingSession.getLastNode())) {
                    if(existingSession.getExpiry() > 0 && existingSession.getExpiry() <= now) {
                        expire.add(candidate);
                    }
                } else {
                    // We've never checked for expiring sessions before, only expire sessions that are long expired
                    if(_lastExpiryCheckTime <= 0) {
                        if(existingSession.getExpiry() + (_gracePeriodSec * 3 * 1000) < now) {
                            expire.add(candidate);
                        }
                    } else if(existingSession.getExpiry() + (_gracePeriodSec * 1000) < now) {
                        // If the session is managed by someone else and we've checked before,
                        // we can expire the session once the grace period has elapsed.
                        expire.add(candidate);
                    }
                }

            } catch(Exception ex) {
                logger.error("Error loading session data for {}", candidate, ex);
            }
        }

        return expire;
    }

    /**
     * @see org.eclipse.jetty.server.session.SessionDataStore#isPassivating()
     */
    public boolean isPassivating() {
        return true;
    }

    /**
     * @see org.eclipse.jetty.server.session.SessionDataStore#exists(String)
     */
    public boolean exists(String sessionId) throws Exception {
        return this.doLoad(sessionId) != null;
    }

    /**
     * @see org.eclipse.jetty.server.session.AbstractSessionDataStore#doLoad(String)
     */
    public SessionData doLoad(String sessionId) throws Exception {
        DynamoSession res = null;
        try {
            res = this.table.getItem(new DynamoSession(sessionId));
        } catch(Exception ex) {
            logger.error("Error loading jetty session {} from DynamoDB.", sessionId, ex);
            delete(sessionId);
        }

        if(res == null) {
            return null;
        } else {
            logger.debug("Loaded session {} from dynamodb", sessionId);
            return res.getSessionData();
        }
    }

    /**
     * @see org.eclipse.jetty.server.session.SessionDataStore#delete(String)
     */
    public boolean delete(String s) throws Exception {
        this.table.deleteItem(new DynamoSession(s));
        return true;
    }

    public static class CurrentTimeProvider {
        public CurrentTimeProvider() {}
        public long getCurrentTime() {
            return System.currentTimeMillis();
        }
    }
}
