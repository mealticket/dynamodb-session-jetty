package com.mealticket.jetty.session.dynamodb;


import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;

public class DynamoSessionDataStoreConfig {
    private AwsCredentials credentials;
    private String endpoint;
    private String regionId;
    private String table;
    private Boolean createTable;
    private Long readCapacityUnits;
    private Long writeCapacityUnits;
    private Boolean removeExpired;

    public DynamoSessionDataStoreConfig() {
        this.credentials = null;
        this.endpoint = null;
        this.regionId = null;
        this.table = null;
        this.readCapacityUnits = 10L;
        this.writeCapacityUnits = 10L;
        this.removeExpired = false;
        this.createTable = false;
    }


    public AwsCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(final String accessKey, final String secretKey) {
        this.credentials = AwsBasicCredentials.builder().accessKeyId(accessKey).secretAccessKey(secretKey).build();
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRegionId() {
        return this.regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getTable() {
        if (this.table == null) {
            return DynamoSession.DEFAULT_TABLE_NAME;
        } else {
            return this.table;
        }
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Boolean getCreateTable() {
        return this.createTable;
    }

    public void setCreateTable(Boolean createTable) {
        this.createTable = createTable;
    }

    public Long getReadCapacityUnits() {
        return readCapacityUnits;
    }

    public void setReadCapacityUnits(long readCapacityUnits) {
        this.readCapacityUnits = readCapacityUnits;
    }

    public Long getWriteCapacityUnits() {
        return writeCapacityUnits;
    }

    public void setWriteCapacityUnits(long writeCapacityUnits) {
        this.writeCapacityUnits = writeCapacityUnits;
    }

    public Boolean getRemoveExpired() {
        return this.removeExpired;
    }

    public void setRemoveExpired(boolean removeExpired) {
        this.removeExpired = removeExpired;
    }

}
