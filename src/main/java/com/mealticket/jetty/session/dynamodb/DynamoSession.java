package com.mealticket.jetty.session.dynamodb;


import org.eclipse.jetty.server.session.SessionData;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*;

import java.util.Map;

@DynamoDbBean
public class DynamoSession {
    public static final String DEFAULT_TABLE_NAME = "Jetty_Session";

    private final SessionData data;

    public DynamoSession() {
        this.data = new SessionData("", "", "", 0, 0, 0, 0);
    }

    public DynamoSession(String id) {
        this.data = new SessionData(id, "", "", 0, 0, 0, 0);
    }

    public DynamoSession(SessionData data) {
        this.data = data;
    }

    @DynamoDbAttribute("session_id")
    @DynamoDbPartitionKey
    public String getId() {
        return this.data.getId();
    }

    public void setId(String id) {
        this.data.setId(id);
    }

    @DynamoDbAttribute("vhost")
    public String getVhost() {
        return this.data.getVhost();
    }

    public void setVhost(String vhost) {
        this.data.setVhost(vhost);
    }

    @DynamoDbAttribute("context_path")
    public String getContextPath() {
        return this.data.getContextPath();
    }

    public void setContextPath(String contextPath) {
        this.data.setContextPath(contextPath);
    }

    @DynamoDbAttribute("created")
    public Long getCreated() {
        return this.data.getCreated();
    }

    public void setCreated(Long created) {
        this.data.setCreated(defaultToZero(created));
    }

    @DynamoDbAttribute("accessed")
    public Long getAccessed() {
        return this.data.getAccessed();
    }

    public void setAccessed(Long accessed) {
        this.data.setAccessed(defaultToZero(accessed));
    }

    @DynamoDbAttribute("last_node")
    public String getLastNode() {
        return this.data.getLastNode();
    }

    public void setLastNode(String lastNode) {
        this.data.setLastNode(lastNode);
    }

    @DynamoDbAttribute("expiry")
    public Long getExpiry() {
        return this.data.getExpiry();
    }

    public void setExpiry(Long expiry) {
        this.data.setExpiry(defaultToZero(expiry));
    }

    // This converts the milliseconds time to a unix timestamp for DynamoDB's TTL
    @DynamoDbAttribute("expiry_ts")
    public Long getExpiryTs() {
        return this.data.getExpiry() / 1000;
    }

    // We don't need to set this on the session data.
    public void setExpiryTs(Long expiry) {}

    @DynamoDbAttribute("max_inactive")
    public Long getMaxInactiveMs() {
        return this.data.getMaxInactiveMs();
    }

    public void setMaxInactiveMs(Long maxInactiveMs) {
        this.data.setMaxInactiveMs(maxInactiveMs);
    }

    @DynamoDbConvertedBy(JettySessionAttributeConverter.class)
    @DynamoDbAttribute("attributes")
    public Map<String, Object> getAttributes() {
        return this.data.getAllAttributes();
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.data.putAllAttributes(attributes);
    }

    @DynamoDbIgnore
    public SessionData getSessionData() {
        return this.data;
    }

    private long defaultToZero(Long l) {
        return l == null ? 0 : l;
    }

    public static class JettySessionDeserializationException extends RuntimeException {
        public JettySessionDeserializationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
