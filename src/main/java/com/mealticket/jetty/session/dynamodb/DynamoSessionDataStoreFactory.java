package com.mealticket.jetty.session.dynamodb;


import org.eclipse.jetty.server.session.AbstractSessionDataStoreFactory;
import org.eclipse.jetty.server.session.SessionDataStore;
import org.eclipse.jetty.server.session.SessionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.internal.waiters.ResponseOrException;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.CreateTableEnhancedRequest;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClientBuilder;
import software.amazon.awssdk.services.dynamodb.model.*;
import software.amazon.awssdk.services.dynamodb.waiters.DynamoDbWaiter;

import java.net.URI;


public class DynamoSessionDataStoreFactory extends AbstractSessionDataStoreFactory {
    private static final Logger logger = LoggerFactory.getLogger(DynamoSessionDataStoreFactory.class);

    private final DynamoSessionDataStoreConfig conf;
    private final DynamoDbClient client;
    private final DynamoDbEnhancedClient enhancedClient;

    public DynamoSessionDataStoreFactory(DynamoSessionDataStoreConfig conf) {
        this(conf, getDynamoDbClient(conf));
    }

    public DynamoSessionDataStoreFactory(final DynamoSessionDataStoreConfig conf, DynamoDbClient client) {
        this(conf, client, getEnhancedDynamoDbClient(client));
    }

    public DynamoSessionDataStoreFactory(final DynamoSessionDataStoreConfig conf, DynamoDbClient client, DynamoDbEnhancedClient enhancedClient) {
        this.conf = conf;
        this.client = client;
        this.enhancedClient = enhancedClient;
    }

    private void createTable() {
        logger.debug("Attempting to create table {} (if it doesn't exist)", this.conf.getTable());

        try {
            this.enhancedClient.table(conf.getTable(), TableSchema.fromBean(DynamoSession.class)).createTable(
                CreateTableEnhancedRequest.builder()
                    .provisionedThroughput(
                        ProvisionedThroughput.builder()
                            .readCapacityUnits(conf.getReadCapacityUnits())
                            .writeCapacityUnits(conf.getWriteCapacityUnits())
                            .build()
                    )
                    .build()
            );

            try (DynamoDbWaiter waiter = DynamoDbWaiter.builder().client(client).build()) { // DynamoDbWaiter is Autocloseable
                ResponseOrException<DescribeTableResponse> response = waiter
                        .waitUntilTableExists(builder -> builder.tableName(conf.getTable()).build())
                        .matched();
                DescribeTableResponse tableDescription = response.response().orElseThrow(
                        () -> new RuntimeException("Customer table was not created."));

                logger.info("{} table was created.", conf.getTable());
            }
        } catch (ResourceInUseException ex) {
            logger.info("Table {} already exists", this.conf.getTable());
        }

        if (this.conf.getRemoveExpired()) {
            this.client.updateTimeToLive(
                UpdateTimeToLiveRequest.builder()
                    .tableName(conf.getTable())
                    .timeToLiveSpecification(
                        TimeToLiveSpecification.builder()
                            .attributeName("expiry")
                            .enabled(true)
                            .build()
                    )
                    .build()
            );
        }
    }

    public static DynamoSessionDataStoreBuilder builder() {
        return new DynamoSessionDataStoreBuilder(new DynamoSessionDataStoreConfig());
    }

    private static AwsCredentialsProvider getDynamoDbCredentials(DynamoSessionDataStoreConfig conf) {
        if (conf.getCredentials() != null) {
            logger.debug("Loading AWS credentials explicitly provided to the factory");
            return StaticCredentialsProvider.create(conf.getCredentials());
        } else {
            logger.debug("Loading security credentials from default AWS credentials provider chain");
            return DefaultCredentialsProvider.create();
        }
    }

    static DynamoDbEnhancedClient getEnhancedDynamoDbClient(DynamoDbClient client) {
        return DynamoDbEnhancedClient.builder().dynamoDbClient(client).build();
    }

    static DynamoDbClient getDynamoDbClient(DynamoSessionDataStoreConfig conf) {

        DynamoDbClientBuilder builder = DynamoDbClient.builder()
            .credentialsProvider(getDynamoDbCredentials(conf));

        if(conf.getRegionId() != null) {
            builder = builder.region(Region.of(conf.getRegionId()));
        }

        if(conf.getEndpoint() != null) {
            builder = builder.endpointOverride(URI.create(conf.getEndpoint()));
        }

        return builder.build();
    }

    public SessionDataStore getSessionDataStore(SessionHandler handler) throws Exception {
        return getDataStore();
    }

    public DynamoSessionDataStore getDataStore() {
        DynamoDbTable<DynamoSession> table = enhancedClient.table(conf.getTable(), TableSchema.fromBean(DynamoSession.class));

        if (this.conf.getCreateTable()) {
            createTable();
        }

        return new DynamoSessionDataStore(table);
    }

    public static class DynamoSessionDataStoreBuilder {
        private DynamoSessionDataStoreConfig config;

        private DynamoSessionDataStoreBuilder(DynamoSessionDataStoreConfig config) {
            this.config = config;
        }

        public DynamoSessionDataStoreBuilder withCredentials(String accessKey, String secretKey) {
            this.config.setCredentials(accessKey, secretKey);
            return this;
        }

        public DynamoSessionDataStoreBuilder withEndpoint(String endpoint) {
            this.config.setEndpoint(endpoint);
            return this;
        }

        public DynamoSessionDataStoreBuilder withRegionId(String regionId) {
            this.config.setRegionId(regionId);
            return this;
        }

        public DynamoSessionDataStoreBuilder withTable(String table) {
            this.config.setTable(table);
            return this;
        }

        public DynamoSessionDataStoreBuilder createTable(Boolean createTable) {
            this.config.setCreateTable(createTable);
            return this;
        }

        public DynamoSessionDataStoreBuilder withReadCapacityUnits(long readCapacityUnits) {
            this.config.setReadCapacityUnits(readCapacityUnits);
            return this;
        }

        public DynamoSessionDataStoreBuilder withWriteCapacityUnits(long writeCapacityUnits) {
            this.config.setWriteCapacityUnits(writeCapacityUnits);
            return this;
        }

        public DynamoSessionDataStoreBuilder withRemoveExpired(boolean removeExpired) {
            this.config.setRemoveExpired(removeExpired);
            return this;
        }

        public DynamoSessionDataStoreConfig getConfig() {
            return this.config;
        }

        public DynamoSessionDataStoreFactory build() {
            return new DynamoSessionDataStoreFactory(this.config);
        }
    }
}
