package com.mealticket.jetty.session.dynamodb;

import org.eclipse.jetty.util.ClassLoadingObjectInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.enhanced.dynamodb.AttributeConverter;
import software.amazon.awssdk.enhanced.dynamodb.AttributeValueType;
import software.amazon.awssdk.enhanced.dynamodb.EnhancedType;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.io.*;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JettySessionAttributeConverter implements AttributeConverter<Map<String,Object>> {
    private static final Logger logger = LoggerFactory.getLogger(JettySessionAttributeConverter.class);

    @Override
    public AttributeValue transformFrom(Map<String, Object> stringObjectMap) {
        return AttributeValue.builder().m(stringObjectMap.entrySet().stream().flatMap(e -> {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(e.getValue());
                return Stream.of(new AbstractMap.SimpleEntry<>(e.getKey(), SdkBytes.fromByteArray(baos.toByteArray())));
            } catch (IOException ex) {
                logger.error("Error serializing jetty session attribute {}", e.getKey(), ex);
                return Stream.empty();
            }
        }).collect(Collectors.toMap(
            AbstractMap.SimpleEntry::getKey,
            e -> AttributeValue.fromB(e.getValue())
        ))).build();
    }

    @Override
    public Map<String, Object> transformTo(AttributeValue attributeValue) {
        if (attributeValue.hasM()) {
            return attributeValue.m().entrySet().stream().flatMap(e -> {
                try {
                    ByteArrayInputStream bis = new ByteArrayInputStream(e.getValue().b().asByteArray());
                    ClassLoadingObjectInputStream ois = new ClassLoadingObjectInputStream(bis);
                    return Stream.of(new AbstractMap.SimpleEntry<>(e.getKey(), ois.readObject()));
                } catch (IOException | ClassNotFoundException ex) {
                    logger.error("Error reading jetty session attribute {}", e.getKey(), ex);
                    return Stream.empty();
                }
            }).collect(Collectors.toMap(
                AbstractMap.SimpleEntry::getKey,
                AbstractMap.SimpleEntry::getValue
            ));
        } else {
            return Collections.emptyMap();
        }
    }

    @Override
    public EnhancedType<Map<String, Object>> type() {
        return EnhancedType.mapOf(String.class, Object.class);
    }

    @Override
    public AttributeValueType attributeValueType() {
        return AttributeValueType.M;
    }
}
