package com.mealticket.jetty.session.dynamodb;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;



public class DynamoSessionDataStoreFactoryTest {
    @Captor
    ArgumentCaptor<CreateTableRequest> createTableCaptor;

    private final TableDescription TEST_TABLE;
    private final DynamoDbTable<DynamoSession> table;

    public DynamoSessionDataStoreFactoryTest() {
        MockitoAnnotations.initMocks(this);
        DynamoDbClient mockClient = mock(DynamoDbClient.class);
        DynamoDbEnhancedClient enhancedClient = mock(DynamoDbEnhancedClient.class);


        TableSchema<DynamoSession> schema = TableSchema.fromBean(DynamoSession.class);


         List<AttributeDefinition> attributes =  schema.tableMetadata().keyAttributes().stream().map(t -> AttributeDefinition.builder()
             .attributeName(t.name())
             .attributeType(t.attributeValueType().toString())
             .build()).collect(Collectors.toList());

        this.table = mock(DynamoDbTable.class);
        this.TEST_TABLE = TableDescription.builder()
            .tableName("test_table")
            .tableStatus(TableStatus.ACTIVE)
            .creationDateTime(Instant.now())
            .tableSizeBytes(8675309L)
            .provisionedThroughput(
                ProvisionedThroughputDescription.builder()
                    .readCapacityUnits(12345L)
                    .writeCapacityUnits(54321L)
                    .build()
            ).keySchema(
                KeySchemaElement.builder().keyType(KeyType.HASH).attributeName("session_id").build()
            ).attributeDefinitions(
                attributes
            ).build();
    }

    @Test
    public void test_create_table_when_configured_without_ttl() {
        int READ_CAPACITY = 60;
        int WRITE_CAPACITY = 50;

        DynamoDbClient mockClient = mock(DynamoDbClient.class);
        DynamoSessionDataStoreConfig config = DynamoSessionDataStoreFactory
            .builder()
            .createTable(true)
            .withReadCapacityUnits(READ_CAPACITY)
            .withWriteCapacityUnits(WRITE_CAPACITY)
            .withTable(TEST_TABLE.tableName())
            .getConfig();

        when(mockClient.describeTable(any(DescribeTableRequest.class)))
            .thenReturn(DescribeTableResponse.builder().table(TEST_TABLE).build());

        when(mockClient.updateTimeToLive(any(UpdateTimeToLiveRequest.class)))
            .thenReturn(UpdateTimeToLiveResponse.builder().build());

        DynamoSessionDataStoreFactory instance = new DynamoSessionDataStoreFactory(config, mockClient);
        instance.getDataStore();

        verify(mockClient, times(1)).createTable(createTableCaptor.capture());
        verify(mockClient, never()).updateTimeToLive(any(UpdateTimeToLiveRequest.class));

        assertEquals(new Long(READ_CAPACITY), createTableCaptor.getValue().provisionedThroughput().readCapacityUnits());
        assertEquals(new Long(WRITE_CAPACITY), createTableCaptor.getValue().provisionedThroughput().writeCapacityUnits());
        assertEquals(TEST_TABLE.tableName(), createTableCaptor.getValue().tableName());
    }

    @Test
    public void test_create_table_when_configured_with_ttl() {
        int READ_CAPACITY = 60;
        int WRITE_CAPACITY = 50;
        DynamoDbClient mockClient = mock(DynamoDbClient.class);
        DynamoSessionDataStoreConfig config = DynamoSessionDataStoreFactory
            .builder()
            .createTable(true)
            .withReadCapacityUnits(READ_CAPACITY)
            .withWriteCapacityUnits(WRITE_CAPACITY)
            .withTable(TEST_TABLE.tableName())
            .withRemoveExpired(true)
            .getConfig();

        when(mockClient.describeTable(any(DescribeTableRequest.class)))
                .thenReturn(DescribeTableResponse.builder().table(TEST_TABLE).build());

        when(mockClient.updateTimeToLive(any(UpdateTimeToLiveRequest.class)))
                .thenReturn(UpdateTimeToLiveResponse.builder().build());

        DynamoSessionDataStoreFactory instance = new DynamoSessionDataStoreFactory(config, mockClient);
        instance.getDataStore();

        verify(mockClient, times(1)).createTable(createTableCaptor.capture());
        verify(mockClient, times(1)).updateTimeToLive(any(UpdateTimeToLiveRequest.class));

        assertEquals(new Long(READ_CAPACITY), createTableCaptor.getValue().provisionedThroughput().readCapacityUnits());
        assertEquals(new Long(WRITE_CAPACITY), createTableCaptor.getValue().provisionedThroughput().writeCapacityUnits());
        assertEquals(TEST_TABLE.tableName(), createTableCaptor.getValue().tableName());
    }

    @Test
    public void test_no_create_table_when_not_configured() {
        DynamoDbClient mockClient = mock(DynamoDbClient.class);
        DynamoSessionDataStoreConfig config = DynamoSessionDataStoreFactory
            .builder()
            .createTable(false)
            .withTable(TEST_TABLE.tableName())
            .getConfig();

        when(mockClient.describeTable(any(DescribeTableRequest.class)))
                .thenReturn(DescribeTableResponse.builder().table(TEST_TABLE).build());
        when(mockClient.updateTimeToLive(any(UpdateTimeToLiveRequest.class)))
                .thenReturn(UpdateTimeToLiveResponse.builder().build());

        DynamoSessionDataStoreFactory instance = new DynamoSessionDataStoreFactory(config, mockClient);
        instance.getDataStore();

        verify(mockClient, never()).createTable(any(CreateTableRequest.class));

        // By default, we don't set the TTL on the table
        verify(mockClient, never()).updateTimeToLive(any(UpdateTimeToLiveRequest.class));
    }
}