package com.mealticket.jetty.session.dynamodb;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.session.SessionContext;
import org.eclipse.jetty.server.session.SessionData;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.collections.Sets;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;

import static org.mockito.Mockito.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class DynamoSessionDataStoreTest {
    @Captor
    ArgumentCaptor<DynamoSession> dynamoSessionCaptor;

    private final int GRACE_PERIOD = 1000;
    private final long FAKE_CURRENT_TIME = 946684800;
    private final DynamoSessionDataStore.CurrentTimeProvider FAKE_CURRENT_TIME_PROVIDER =
        new DynamoSessionDataStore.CurrentTimeProvider() {
            public long getCurrentTime() {
                return FAKE_CURRENT_TIME;
            }
        };

    private SessionData getTestSession() {
        SessionData data = new SessionData("session-that-exists", "test-cc", "test", 0, 0, 0, 0);
        data.setAttribute("string-attr", "This is a String attribute");
        data.setAttribute("long-attr", Long.MAX_VALUE);
        data.setAttribute("double-attr", Math.PI);
        Map<String, Integer> testObj = new LinkedHashMap<>();
        testObj.put("one", 1);
        testObj.put("two", 2);
        testObj.put("three", 3);

        data.setAttribute("object-attr", testObj);
        return data;
    }

    public DynamoSessionDataStoreTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_doStore() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        instance.store(testSession.getId(), testSession);

        verify(mockedTable).putItem(dynamoSessionCaptor.capture());

        assertEquals(testSession, dynamoSessionCaptor.getValue().getSessionData());
    }

    @Test
    public void test_doGetExpired_local_session_only() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(null);


        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        Set<String> expected = Sets.newSet(testSession.getId());
        Set<String> result = instance.getExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertEquals(expected, result);
    }

    @Test
    public void test_doGetExpired_locally_managed_session_expiring_now() throws Exception {
        final String THIS_NODE = "this_node";

        SessionData testSession = getTestSession();
        testSession.setExpiry(FAKE_CURRENT_TIME);
        testSession.setLastNode(THIS_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> expected = Sets.newSet(testSession.getId());
        Set<String> result = instance.doGetExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertEquals(expected, result);
    }

    @Test
    public void test_doGetExpired_locally_managed_session_expired() throws Exception {
        final String THIS_NODE = "this_node";

        SessionData testSession = getTestSession();
        testSession.setExpiry(FAKE_CURRENT_TIME - 1);
        testSession.setLastNode(THIS_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(Key.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> expected = Sets.newSet(testSession.getId());
        Set<String> result = instance.doGetExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertEquals(expected, result);
    }

    @Test
    public void test_doGetExpired_locally_managed_session_not_expired() throws Exception {
        final String THIS_NODE = "this_node";

        SessionData testSession = getTestSession();
        // It expires in the "future"
        testSession.setExpiry(FAKE_CURRENT_TIME + 1);
        testSession.setLastNode(THIS_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> result = instance.doGetExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertTrue(result.isEmpty());
    }

    @Test
    public void test_doGetExpired_remotely_managed_session_not_expired() throws Exception {
        final String THIS_NODE = "this_node";
        final String OTHER_NODE = "other_node";

        SessionData testSession = getTestSession();
        // It expires in the "future"
        testSession.setExpiry(FAKE_CURRENT_TIME + 1);
        testSession.setLastNode(OTHER_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> result = instance.getExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertTrue(result.isEmpty());
    }

    @Test
    public void test_doGetExpired_remotely_managed_session_expired() throws Exception {
        final String THIS_NODE = "this_node";
        final String OTHER_NODE = "other_node";

        SessionData testSession = getTestSession();
        testSession.setExpiry(FAKE_CURRENT_TIME);
        testSession.setLastNode(OTHER_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> result = instance.getExpired(Sets.newSet(testSession.getId()));

        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertTrue(result.isEmpty());
    }

    @Test
    public void test_doGetExpired_remotely_managed_session_beyond_grace() throws Exception {
        final String THIS_NODE = "this_node";
        final String OTHER_NODE = "other_node";

        SessionData testSession = getTestSession();
        // It's just barely past the grace period
        testSession.setExpiry(FAKE_CURRENT_TIME - (GRACE_PERIOD * 1000) - 1);
        testSession.setLastNode(OTHER_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        // First check, unless we are WAY past the grace period, it shouldn't be a candidate for expiration
        Set<String> result = instance.getExpired(Sets.newSet(testSession.getId()));
        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertTrue(result.isEmpty());

        // Second check, as long as we're past the grace period, it should be a candidate for expiration
        result = instance.getExpired(Sets.newSet(testSession.getId()));
        Set<String> expected = Sets.newSet(testSession.getId());
        assertEquals(expected, result);
    }

    @Test
    public void test_doGetExpired_remotely_managed_way_beyond_grace() throws Exception {
        final String THIS_NODE = "this_node";
        final String OTHER_NODE = "other_node";

        SessionData testSession = getTestSession();
        // way beyond the grace period
        testSession.setExpiry(FAKE_CURRENT_TIME - (GRACE_PERIOD * 1000 * 3) - 1);
        testSession.setLastNode(OTHER_NODE);

        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionContext sessionContext = mock(SessionContext.class);
        when(sessionContext.getCanonicalContextPath()).thenReturn(testSession.getContextPath());
        when(sessionContext.getVhost()).thenReturn(testSession.getVhost());
        when(sessionContext.getWorkerName()).thenReturn(THIS_NODE);

        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable, FAKE_CURRENT_TIME_PROVIDER);
        instance.setGracePeriodSec(GRACE_PERIOD);
        instance.initialize(sessionContext);
        instance.start();

        Set<String> expected = Sets.newSet(testSession.getId());

        // First check. With the current time way beyond the grace period, it should be a candidate for expiration
        Set<String> result = instance.getExpired(Sets.newSet(testSession.getId()));
        verify(mockedTable, times(1)).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertEquals(expected, result);
    }

    @Test
    public void test_exists_when_session_exists() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        assertTrue(instance.exists(testSession.getId()));
        verify(mockedTable).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());

    }

    @Test
    public void test_exists_when_session_does_not_exist() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(null);

        assertFalse(instance.exists(testSession.getId()));
        verify(mockedTable).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
    }

    @Test
    public void test_load() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        when(mockedTable.getItem(any(DynamoSession.class))).thenReturn(new DynamoSession(testSession));

        SessionData result = instance.doLoad(testSession.getId());
        verify(mockedTable).getItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
        assertEquals(testSession, result);
    }

    @Test
    public void test_delete() throws Exception {
        SessionData testSession = getTestSession();
        DynamoDbTable<DynamoSession> mockedTable = mock(DynamoDbTable.class);
        DynamoSessionDataStore instance = new DynamoSessionDataStore(mockedTable);
        instance.initialize(new SessionContext("TEST", null));
        instance.start();

        instance.delete(testSession.getId());
        verify(mockedTable).deleteItem(dynamoSessionCaptor.capture());
        assertEquals(testSession.getId(), dynamoSessionCaptor.getValue().getId());
    }

}