package com.mealticket.jetty.session.dynamodb;

import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.amazonaws.util.IOUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.session.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Base64;
import java.util.Collections;

import static com.amazonaws.SDKGlobalConfiguration.*;
import static org.junit.Assert.*;

@Category(IntegrationTest.class)
public class DynamoSessionDataStoreIT {
    private static DynamoDBProxyServer dynamoDbServer;
    private static String dynamoDbPort;
    private static Integer server1Port;
    private static Server server1;
    private static Integer server2Port;
    private static Server server2;
    private static DynamoSessionDataStoreFactory dataStoreFactory;
    private static final Logger logger = LoggerFactory.getLogger(DynamoSessionDataStoreIT.class);
    private static final String ACCESS_KEY = "FakeAccessKey";
    private static final String SECRET_KEY = "FakeSecretKey";
    private static final Region REGION = Region.US_WEST_2;

    public DynamoSessionDataStoreIT() {}

    @BeforeClass
    public static void setup() throws Exception {
        System.setProperty(ACCESS_KEY_SYSTEM_PROPERTY, ACCESS_KEY);
        System.setProperty(SECRET_KEY_SYSTEM_PROPERTY, SECRET_KEY);
        System.setProperty(AWS_REGION_SYSTEM_PROPERTY, REGION.id());

        dynamoDbPort = System.getProperty("test.dynamodb.port", "8002");
        dynamoDbServer = ServerRunner.createServerFromCommandLineArgs(
            new String[] {"-inMemory", "-port", dynamoDbPort}
        );
        logger.info("sqlite4java.library.path is {}", System.getProperty("sqlite4java.library.path"));
        logger.info("Starting in-memory dynamo db dynamoDbServer on port {}", dynamoDbPort);
        dynamoDbServer.start();

        String TABLE_NAME = "dynamo_session_data_store_it";
        dataStoreFactory = DynamoSessionDataStoreFactory.builder()
                .withEndpoint(String.format("http://localhost:%s", dynamoDbPort))
                .withRegionId(REGION.id())
                .withCredentials(ACCESS_KEY, SECRET_KEY)
                .withWriteCapacityUnits(10)
                .withReadCapacityUnits(10)
                .withTable(TABLE_NAME)
                .createTable(true)
                .build();


        server1Port = Integer.parseInt(System.getProperty("test.jetty.port1", "8003"));
        server2Port = Integer.parseInt(System.getProperty("test.jetty.port2", "8004"));

        server1 = getTestServer(server1Port);
        server2 = getTestServer(server2Port);
    }

    private SessionData getTestSessionData() {
        return new SessionData(
            "test_session_id",
            "ctx",
            "localhost",
            System.currentTimeMillis(),
            0,
            0,
            0
        );
    }

    @Test
    public void test_storing_and_retrieving_session() throws Exception {
        String TABLE_NAME = "test_storing_and_retrieving_session";
        DynamoSessionDataStoreFactory dataStoreFactory = DynamoSessionDataStoreFactory.builder()
            .withEndpoint(String.format("http://localhost:%s", dynamoDbPort))
            .withRegionId(REGION.id())
            .withCredentials(ACCESS_KEY, SECRET_KEY)
            .withWriteCapacityUnits(10)
            .withReadCapacityUnits(10)
            .withTable(TABLE_NAME)
            .createTable(true)
            .build();

        DynamoSessionDataStore ds = dataStoreFactory.getDataStore();
        ds.initialize(new SessionContext("TEST", null));
        ds.start();

        SessionData testSession = getTestSessionData();
        ds.doStore(testSession.getId(), testSession, 0);

        assertTrue(ds.exists(testSession.getId()));

        SessionData result = ds.load(testSession.getId());
        assertEquals(testSession.getId(), result.getId());

        ds.delete(testSession.getId());
        assertFalse(ds.exists(testSession.getId()));
    }

    /**
     * This test simulates a non-sticky session configuration (writing to the session on both servers). Since it is
     * not using sticky sessions, we need a NullSessionCache
     * @see <a href="http://www.eclipse.org/jetty/documentation/9.4.5.v20170502/sessions-details.html#_use_cases">http://www.eclipse.org/jetty/documentation/9.4.5.v20170502/sessions-details.html#_use_cases</a>
     * @throws Exception
     */
    @Test
    public void test_session_handling_between_servers() throws Exception {
        String testEndpoint = "http://localhost:%d/test-session-handling-between-servers";
        String expected1 = "FIRST VALUE";
        CookieStore cookies = new BasicCookieStore();
        Executor executor = Executor.newInstance().use(cookies);

        // Set the value to the session on dynamoDbServer 1
        assertEquals(
            200,
            executor.execute(
                org.apache.http.client.fluent.Request
                .Post(String.format(testEndpoint, server1Port))
                .bodyString(expected1, ContentType.DEFAULT_TEXT)
            ).returnResponse()
             .getStatusLine()
             .getStatusCode()
        );
        Thread.sleep(200);

        // Read the value from the session on dynamoDbServer 2
        assertEquals(
            expected1,
            executor.execute(
                org.apache.http.client.fluent.Request
                .Get(String.format(testEndpoint, server2Port))
            ).returnContent()
             .asString()
        );
        Thread.sleep(200);

        String expected2 = "SECOND VALUE";

        // Set the value to the session on dynamoDbServer 2
        assertEquals(
            200,
            executor.execute(
                org.apache.http.client.fluent.Request
                .Post(String.format(testEndpoint, server2Port))
                .bodyString(expected2, ContentType.DEFAULT_TEXT)
            ).returnResponse()
             .getStatusLine()
             .getStatusCode()
        );
        Thread.sleep(200);

        // Read the new value from the session on dynamoDbServer2
        assertEquals(
            expected2,
            executor.execute(
                org.apache.http.client.fluent.Request
                .Get(String.format(testEndpoint, server2Port))
            ).returnContent()
             .asString()
        );
        Thread.sleep(200);

        // Read the new value from the session on dynamoDbServer1
        assertEquals(
            expected2,
            executor.execute(
                org.apache.http.client.fluent.Request
                .Get(String.format(testEndpoint, server1Port))
            ).returnContent()
             .asString()
        );

    }

    /**
     * This is a test handler. It lets you store/retrieve stuff from the session for testing purposes.
     */
    private static class TestHandler extends AbstractHandler {

        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            HttpSession session = request.getSession(true);
            logger.info("Session {} fields {}", session.getId(), String.join(", ", Collections.list(session.getAttributeNames())));

            if("POST".equals(request.getMethod())) {
                String body = IOUtils.toString(request.getInputStream());
                session.setAttribute(Base64.getEncoder().encodeToString(target.getBytes()), body);
                response.setStatus(200);
                baseRequest.setHandled(true);
                response.getWriter().print("Successfully added item '" + body + "' to session.");
            } else {
                Object value = session.getAttribute(Base64.getEncoder().encodeToString(target.getBytes()));
                if(value == null) {
                    response.setStatus(400);
                    baseRequest.setHandled(true);
                    response.getWriter().print("Unable to location session value for " + target);
                } else {
                    response.setStatus(200);
                    baseRequest.setHandled(true);
                    response.getWriter().print((String) value);
                }
            }
        }
    }

    private static Server getTestServer(int port) throws Exception {
        Server server = new Server(port);

        server.addBean(dataStoreFactory);
        server.addBean(new NullSessionCacheFactory());

        // Specify the Session ID Manager
        DefaultSessionIdManager idmanager = new DefaultSessionIdManager(server);
        server.setSessionIdManager(idmanager);

        // Sessions are bound to a context.
        ContextHandler context = new ContextHandler("/");
        server.setHandler(context);

        // Create the SessionHandler (wrapper) to handle the sessions
        SessionHandler sessionHandler = new SessionHandler();
        sessionHandler.setSessionIdManager(idmanager);
        context.setHandler(sessionHandler);

        server.setHandler(sessionHandler);
        sessionHandler.setHandler(new TestHandler());
        logger.info("Starting embedded jetty instance on port {}", port);
        server.start();
        return server;
    }

    @AfterClass
    public static void teardown() throws Exception {
        if(server1 != null) {
            try {
                server1.stop();
            } catch (Throwable e) {
                logger.error("Error stopping jetty server 1", e);
            }
        }

        if(server2 != null) {
            try {
                server2.stop();
            } catch (Throwable e) {
                logger.error("Error stopping jetty server 2", e);
            }
        }

        if(dynamoDbServer != null) {
            try {
                dynamoDbServer.stop();
            } catch (Throwable e) {
                logger.error("Error stopping embedded dynamo db server", e);
            }
        }
    }

}
