DynamoDB Persistence for Jetty Session Data
===========================================

This project allows one to use [AWS DynamoDB] 
for persistence/replication of Jetty's session data. [AWS DynamoDB] is a fast, affordable, scalable 
and highly-available nosql database service and as such is a great option for session storage and
replication for Jetty instances deployed to [AWS].


Installation
-------------------------------------------
To use dynamodb-session-jetty, you have to make the library and it's dependencies available to 
the Jetty server. The library is available to download [here](https://bitbucket.org/mealticket/dynamodb-session-jetty/downloads/?tab=downloads).

You have the option of downloading the simple jar, which will require all of the dependencies to be available in the
servers classpath, or you can download the fat jar, `*-all.jar`, which includes a copy of all of the dependencies
(shaded, so you can still use Amazon AWS libraries in your web applications).

If you are using the fat jar, you may exclude the dependencies of this jar.
```xml
<!-- Only pull the fat jar, not the dependencies -->
<dependency>
    <groupId>com.mealticket</groupId>
    <artifactId>dynamodb-session-jetty</artifactId>
    <version>1.0.5</version>
    <classifier>all</classifier>
    <exclusions>
        <exclusion>
            <groupId>*</groupId>
            <artifactId>*</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

Configuration
-------------------------------------------
To configure your jetty server to use the DynamoSessionDataStore, you need to provide the DynamoSessionDataStoreFactory
to your server. 

Here's an example of providing the DynamoSessionDataStoreFactory to an embedded Jetty server.

```java
    Server server = new Server(port);
    DynamoSessionDataStoreFactory dataStoreFactory = DynamoSessionDataStoreFactory.builder()
        .withWriteCapacityUnits(10)
        .withReadCapacityUnits(10)
        .withRegionId("us-west-2")
        .withTable("TESTTABLE")
        .createTable(true)
        .withRemoveExpired(true)
        .build();

    server.addBean(dataStoreFactory);
    
    // If you are using sticky sessions, adding the NullSessionCacheFactory
    // to your server config is not necessary
    server.addBean(new NullSessionCacheFactory());
```

Here's the same configuration if you were to provide it to the Jetty server using XML.
```xml
<Configure id="Server" class="org.eclipse.jetty.server.Server">
    <Call name="addBean">
        <Arg>
            <New id="sessionCacheFactory" class="org.eclipse.jetty.server.session.NullSessionCacheFactory"/>
        </Arg>
    </Call>
    <Call name="addBean">
       <Arg>
            <New id="sessionDataStoreFactory" class="com.mealticket.jetty.session.dynamodb.DynamoSessionDataStoreFactory">
                <Arg>
                    <New class="com.mealticket.jetty.session.dynamodb.DynamoSessionDataStoreConfig">
                        <Set name="table">TESTTABLE</Set>
                        <Set name="regionId">us-west-2</Set>
                        <Set name="createTable">true</Set>
                        <Set name="createTable">true</Set>
                        <Set name="removeExpired">true</Set>
                        <Set name="readCapacityUnits">10</Set>
                        <Set name="writeCapacityUnits">10</Set>
                    </New>
                </Arg>
            </New>
       </Arg>
    </Call>
</Configure>
```

[AWS]: https://aws.amazon.com/ "Amazon AWS"
[AWS DynamoDB]: https://aws.amazon.com/dynamodb/  "Amazon DynamoDB"